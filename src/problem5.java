
public class problem5 {
	
	public static void main(String[]args) {
		
		String word = removeDoubledLetters("bookkeeper");
		System.out.println(word);
		word = removeDoubledLetters("tresidder");
		System.out.println(word);
		
	}
	
	private static String removeDoubledLetters(String str) {
		
		String result = "";
		for (int i = 0; i < str.length(); i++) {
		char ch = str.charAt(i);
		if (i == 0 || ch != str.charAt(i - 1)) {
		result += ch;
		}
		}
		return result;
		}
	}
