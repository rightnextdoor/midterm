import stanford.karel.*;

public class BlankKarel extends SuperKarel {

	public void run() {
		// put karel in the starting position
		startKarel();
		// move Karel and put beeper in a row 
		for (int i = 0; i < 4; i++) {
			moveKarel();
			moveNext();
		}
	}
	
	private void startKarel() {
		turnLeft();
		move();
		turnRight();
	}
	
	private void moveKarel() {
		move();
		// see if karel can move forward and place a beeper
		while (frontIsClear()) {
			if (noBeepersPresent()) {
				putBeeper();
			}
			move();
		}
	}
	private void moveNext() {
		// move Karel to the next position to move forward
		turnRight();
		move();
		turnRight();
		move();
		turnRight();
		
	}
}
