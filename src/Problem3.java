import java.util.Scanner;
public class Problem3 {
	
	private static final int SENTINEL = 0;
	
	public static void main(String[]args) {
		
		System.out.println("This program finds the two largest integers in a /n"
				+ " list. Enter values, one per line, using a " + SENTINEL + " to singnal /n"
				+" the end of the list.");
		
		// num1 and num2 start less then 0 
		int num1 = -1;
		int num2 = -1;
		
		while(true) {
			Scanner input = new Scanner(System.in);
			System.out.print("Enter a postitive number: ");
			int check = input.nextInt();
			
			if(check == SENTINEL)break;
			if(check > 0) {
				if (check > num1) {
					num1 = check;
				} else if (check > num2) {
					num2 = check;
				}
			} else {
				System.out.println("You didn't enter a postitive number");
			}
		}
		System.out.println("The largest value is " + num1);
		System.out.println("The second largest value is " + num2);

	}

}
