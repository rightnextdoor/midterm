import acm.graphics.*;
import acm.program.*;
import java.awt.*;
import java.awt.event.*;

public class problem4 extends GraphicsProgram {
	
	private static final int SQSIZE = 250;
	private static final int NCOLS = 7;
	private static final int NROWS = 3;
	
	public static final int APPLICATION_WIDTH = NCOLS  * SQSIZE;
	public static final int APPLICATION_HEIGHT = NROWS * SQSIZE;
	
	
	public void run() {
		
		frog = new GImage("frog.jpg");
		frog.scale(0.05);
		
		xCoord = (NCOLS  / 2 + 0.5) * SQSIZE;
		yCoord = (NROWS - 0.5) * SQSIZE;
		add(frog, xCoord - frog.getWidth() / 2, yCoord - frog.getHeight() / 2);
		
		addMouseListeners();
	}
	

	
	private void moveFrog(double x, double y) {
		if (World(xCoord + x, yCoord + y)) {
			xCoord += x;
			yCoord += y;
			frog.move(x, y);
		}
	}
	
	private boolean World(double x, double y) {
		return (x >= 0 && x <= NCOLS  * SQSIZE && 
				y >= 0 && y <= NROWS * SQSIZE);
	}
	
	public void mouseClicked(MouseEvent e) {
		double x = e.getX();
		double y = e.getY();
		if (Math.abs(x - xCoord) > Math.abs(y - yCoord)) {
			if (x > xCoord) {
				moveFrog(SQSIZE, 0);
			} else {
				moveFrog(-SQSIZE , 0);
			}
		} else {
			if (y > yCoord) {
				moveFrog(0, SQSIZE);
			} else {
				moveFrog(0, -SQSIZE);
			}
		}
	}
	
	private GImage frog;
	private double xCoord;
	private double yCoord;

}
